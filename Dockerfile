From tomcat:8.0.51-jre8-alpine
RUN rm -rf /usr/local/tomcat/webapps/*
COPY ./target/Spring3HibernateApp.war /usr/local/tomcat/webapps/Spring3HibernateApp.war
EXPOSE 8080
CMD ["catalina.sh","run"]